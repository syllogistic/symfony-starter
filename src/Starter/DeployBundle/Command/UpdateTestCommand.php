<?php
namespace Starter\DeployBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Starter\DeployBundle\BaseCommand;

class UpdateTestCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('deploy:update:test')
            ->setDescription('Runs automated tests');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(!file_exists('phpunit.phar'))
            die('Could not test. Please download phpunit.phar from https://phar.phpunit.de/phpunit.phar');

        // Run automated tests
        $this->checkAndExec('php phpunit.phar -c app/');
    }
}