<?php
namespace Starter\DeployBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Starter\DeployBundle\BaseCommand;

class UpdateGitCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('deploy:update:git')
            ->setDescription('Update source code from Git');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Update source code (this has to be done before going offline)
        $this->checkAndExec("git fetch");
        $this->checkAndExec("git checkout $branch");
        $this->checkAndExec("git pull origin $branch");

        // Output commit version
        $this->checkAndExec('git log -1 --format=%ci > last-commit.txt');
    }
}