<?php
namespace Starter\DeployBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Starter\DeployBundle\BaseCommand;

class UpdateComposerCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('deploy:update:composer')
            ->setDescription('Updates composer and runs "composer install"');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(!file_exists('composer.phar'))
            die('Could not update. Please download composer.phar from https://getcomposer.org/download/');

        // Update composer if it's more than 25 days old
        if (time()-filemtime('composer.phar') > 25 * 24 * 3600)
            $this->checkAndExec("php composer.phar self-update");

        // Update vendor packages
        if($this->env == 'prod')
            $this->checkAndExec('php composer.phar install --no-dev --no-interaction --optimize-autoloader');
        else
            system('php composer.phar install');
    }
}