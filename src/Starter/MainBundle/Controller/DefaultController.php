<?php

namespace Starter\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('StarterMainBundle:Default:index.html.twig');
    }
}
