<?php

namespace Starter\OAuthBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class StarterOAuthBundle extends Bundle
{
    public function getParent()
    {
        return 'HWIOAuthBundle';
    }
}