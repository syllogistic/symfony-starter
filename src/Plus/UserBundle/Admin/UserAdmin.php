<?php
namespace Plus\UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class UserAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('email', 'text', array('label' => 'Email Address'))
            ->add('full_name')
            ->add('roles','choice',array('choices'=>
                array(
                    'ROLE_ADMIN'=>'Admin',
                ), 'multiple'=>true,'required'=>false ))
            ->add('picture_url')
        ;

        if($this->isGranted('ADMIN'))
        {
            $formMapper
                ->add('enabled', null, array('required'=>false))
                ->add('locked', null, array('required'=>false))
            ;
        }
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('email')
            ->add('full_name')
            ->add('enabled')
            ->add('locked')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('email')
            ->add('full_name')
            ->add('last_login', 'datetime')
        ;
    }
}