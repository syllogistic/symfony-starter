<?php

namespace Plus\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PlusMainBundle:Default:index.html.twig');
    }
}
