<?php

namespace Starter\DeployBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class StarterDeployExtension extends Extension {
    public function load(array $configs, ContainerBuilder $container)
    {
        $config = array();
        // let resources override the previous set value
        foreach ($configs as $subConfig) {
            $config = array_merge($config, $subConfig);
        }

        $container->setParameter('starter_deploy.entity_bundles',
            isset($config['entity_bundles']) ? implode(' ', $config['entity_bundles']) : null);
    }
} 