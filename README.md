Symfony Starter
=====

Adds the following to [Symfony Plus](https://bitbucket.org/syllogistic/symfony-plus):

1. Bootstrap and basic styling
1. Deployment tools