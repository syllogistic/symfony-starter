<?php

namespace Starter\DeployBundle;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BaseCommand extends ContainerAwareCommand {
    protected $env = null;

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->env = $this->getContainer()->getParameter("kernel.environment");
    }

    protected function checkAndExec($command)
    {
        $return_var = -1;
        $ret = system($command, $return_var);
        if($ret === false || $return_var != 0)
        {
            $this->preExit();
            $msg = "$command FAILED [$return_var] - Aborting ".$this->getName();
            if($this->env == 'prod')
                throw new \Exception($msg);

            die("\n".$msg."\n");
        }
        return $return_var;
    }

    /**
     * Cleanup before exit
     */
    protected function preExit()
    {
        //...
    }
} 