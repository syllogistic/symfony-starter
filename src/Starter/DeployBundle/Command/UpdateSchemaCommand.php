<?php
namespace Starter\DeployBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Starter\DeployBundle\BaseCommand;

class UpdateSchemaCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('deploy:update:schema')
            ->setDescription('Updates doctrine\'s database schema and generates entities');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityBundles = $this->getContainer()->getParameter('starter_deploy.entity_bundles');

        if($this->env == 'dev' && $entityBundles)
        {
            // Generate Entity functions
            $this->checkAndExec("php app/console doctrine:generate:entities $entityBundles");
        }

        // Update database schema
        $this->checkAndExec('php app/console doctrine:schema:update --force');
    }
}