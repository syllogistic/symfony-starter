<?php

namespace Starter\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('full_name', null, array('required'=>false));
        
        parent::buildForm($builder, $options);
 
        $builder
            ->remove('username')
            ->add('picture_url', 'hidden', array('required'=>false));
    }

    public function getName()
    {
        return 'starter_user_registration';
    }    
}
