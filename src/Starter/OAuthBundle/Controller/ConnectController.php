<?php

/*
 * Overrides HWI\Bundle\OAuthBundle\Controller\ConnectController
 */

namespace Starter\OAuthBundle\Controller;

use HWI\Bundle\OAuthBundle\OAuth\ResourceOwnerInterface;
use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;
use HWI\Bundle\OAuthBundle\Controller\ConnectController as BaseController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

use Rumie\UserBundle\Entity\User;

/**
 * ConnectController
 *
 * @author Alexander <iam.asm89@gmail.com>
 */
class ConnectController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function registrationAction(Request $request, $key)
    {
        // Get the user's email address
        $em = $this->container->get('doctrine.orm.entity_manager');
        $session = $request->getSession();
        $error = $session->get('_hwi_oauth.registration_error.'.$key);
        $userInformation = $this
            ->getResourceOwnerByName($error->getResourceOwnerName())
            ->getUserInformation($error->getRawToken());
        $userManager = $this->container->get('fos_user.user_manager');
        $regEmail = $userInformation->getEmail();
        $userByUsername = $userManager->findUserByUsername($regEmail);

        // Show a nice error message if the user's email address has already been registered
        if($userByUsername)
        {
            return $this->container->get('templating')->renderResponse('HWIOAuthBundle:Connect:already_registered.html.' . $this->getTemplatingEngine(),
                array('email'=>$regEmail));
        }

        $ret = parent::registrationAction($request, $key);

        // Do any additional processing here

        return $ret;
    }
}
