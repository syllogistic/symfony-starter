<?php
namespace Starter\DeployBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Starter\DeployBundle\BaseCommand;

class UpdateCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('deploy:update')
            ->setDescription('Update vendor, schema, and run tests');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->offline();
        $this->checkAndExec('php app/console deploy:update:composer');
        $this->online();
        $this->checkAndExec('php app/console deploy:update:git');
        $this->offline();
        $this->checkAndExec('php app/console deploy:update:schema');

        if($this->env == 'prod')
        {
            // Clear prod cache
            // NOTE: We don't check the exit code since it sometimes fails to remove a directory
            // NOTE: The dev cache is cleared by 'composer install'
            system('php app/console cache:clear --env=prod --no-debug --no-warmup');

            // Cache bust css/javascript files
            //$this->checkAndExec('php app/console deploy:cache-bust');
        }

        $this->online();
        $this->checkAndExec('php app/console deploy:update:test');
    }

    protected function preExit()
    {
        parent::preExit();
        $this->online();
    }

    private function offline()
    {
        if($this->env == 'prod')
        {
            // Backup the real app.php
            copy($this->rootDir.'/../web/app.php', $this->rootDir.'/../web/app.online.php');

            // Take site offline
            copy($this->rootDir.'/../web/app.offline.php', $this->rootDir.'/../web/app.php');
        }
    }

    private function online()
    {
        if($this->env == 'prod')
        {
            // Bring site back online and remove the app.php backup
            if(file_exists($this->rootDir.'/../web/app.online.php'))
                copy($this->rootDir.'/../web/app.online.php', $this->rootDir.'/../web/app.php');
            if(file_exists($this->rootDir.'/../web/app.online.php'))
                unlink($this->rootDir.'/../web/app.online.php');
        }
    }

}